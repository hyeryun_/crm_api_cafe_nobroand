﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Dapper;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;

using CRM_API_Cafe_NoBroand.Models;

using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

namespace CRM_API_Cafe_NoBroand.Helpers
{
    public class AppPrivateLessonGradingDataAccess
    {
        #region 밀크T 첨삭 교사 채점 화면 - 채점/검수 상태 업데이트
        public void Api_Set_Edits_Cheomsak_Status(P_MODEL_USP_EDITS_CHEOMSAK_STATUS p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "USP_EDITS_CHEOMSAK_STATUS";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                oParam.Add("@IN_STATUS", p_model.IN_STATUS.ToString());

                Dbcon.Query(procedure, oParam, commandType: CommandType.StoredProcedure);
                Dbcon.Close();
            }
        }
        #endregion



        #region 밀크T 첨삭 교사 채점 화면 - 차시 + 문제 INFO

        public D_MODEL_CHEOMSAK_LECTURE_QUESTION_INFO Api_Get_Edits_Cheomsak_Lecture_Question_Info(P_MODEL_Global_PrivateLessonGrading_type01 p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                
                // 차시정보
                var lectureInfo = Dbcon.Query<D_MODEL_USP_APP_CHEOMSAK_LECTURE_READ>("USP_APP_CHEOMSAK_LECTURE_READ", oParam, commandType: CommandType.StoredProcedure).First();

                // 문항정보
                var questionInfo = Dbcon.Query<D_MODEL_USP_APP_CHEOMSAK_LECTURE_QUESTION_READ>("USP_APP_CHEOMSAK_LECTURE_QUESTION_READ", oParam, commandType: CommandType.StoredProcedure).ToList();

                // 첨삭가이드
                var guideInfo = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ_NEW>("USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ_NEW", oParam, commandType: CommandType.StoredProcedure).ToList();

                // 첨삭제출정보
                var answerInfo = Dbcon.Query<D_MODEL_USP_APP_CHEOMSAK_ANSWER_USERINFO_READ>("USP_APP_CHEOMSAK_ANSWER_USERINFO_READ", oParam, commandType: CommandType.StoredProcedure).ToList();

                var resultQuestionInfo = (from Q in questionInfo
                                         join A in answerInfo on Q.qIdx equals A.qIdx into _A
                                         from A in _A.DefaultIfEmpty(new D_MODEL_USP_APP_CHEOMSAK_ANSWER_USERINFO_READ())
                                         select new D_MODEL_CHEOMSAK_QUESTION_INFO
                                         {                                             
                                             qIdx = Q.qIdx,
                                             SType = Q.SType,
                                             qWUrl = Q.qWUrl,
                                             DataUri = A != null ? A.DataUri : "",
                                             Datatext = A != null ? A.datatext : "",
                                             Guides = guideInfo.Where(x => x.qIdx == Q.qIdx).ToList<D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ_NEW>(),
                                             Correct = A != null ? A.correct : "",
                                             AiTutorAnswer = A != null ? A.aitutoranswer : "",
                                             AiTutorComment = A != null ? A.aitutorcomment : "",
                                             AiTutorCorrect = A != null ? A.aitutorcorrect : "",
                                             S_Answer1 = Q.S_Answer1,
                                             S_Answer2 = Q.S_Answer2,
                                             S_Answer3 = Q.S_Answer3,
                                             VideoUrl = Q.VideoUrl
                                         }).ToList();

                var resultlist = new D_MODEL_CHEOMSAK_LECTURE_QUESTION_INFO();

                resultlist.Lecture = lectureInfo;
                resultlist.Question = resultQuestionInfo;

                Dbcon.Close();

                return resultlist;

            }
        }

        #endregion

        #region 밀크T 첨삭 교사 채점 화면 - 채점 검수

        public int Api_Get_Edits_Cheomsak_PartTeacher_Eval(P_MODEL_USP_EDITS_CHEOMSAK_PARTTEACHER_EVAL p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_PARTTEACHER_EVAL_NEW";
                int intReturn = -1;

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID);
                oParam.Add("@IN_CCODE", p_model.IN_CCODE);
                oParam.Add("@IN_QIDX", p_model.IN_QIDX);
                oParam.Add("@IN_TeacherID", p_model.IN_TeacherID);
                oParam.Add("@OUT_Return", dbType: DbType.Int32, direction: ParameterDirection.Output);                

                Dbcon.Execute(procedure, oParam, commandType: CommandType.StoredProcedure);

                intReturn = Convert.ToInt32(oParam.Get<int>("@OUT_Return"));

                return intReturn;
            }
        }


        #endregion


    }

}
