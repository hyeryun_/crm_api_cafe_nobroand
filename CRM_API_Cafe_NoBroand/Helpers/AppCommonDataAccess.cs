﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Dapper;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;

using CRM_API_Cafe_NoBroand.Models;

namespace CRM_API_Cafe_NoBroand.Helpers
{
    public class AppCommonDataAccess
    {
        #region 토큰 발행 / 로그인
        public IEnumerable<T> Get_api_authenticate<T>(IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                //var output = Dbcon.Query("select top 1 UserName from TBL_APP_TeacherInfo");    // 일반쿼리
                //var output = conexao.Query<Indicador>("SELECT 1");    // 모델사용

                //var procedure = "dbo.USP_APP_CHEOMSAK_LOGIN";
                //var sp_param = new DynamicParameters();
                //sp_param.Add("@in_UserName", "yooby");
                //sp_param.Add("@in_Pwd", "qwerty1234*");

                // Dbcon.Execute< Authenticate_loginModel>(query, sp_param, commandType: CommandType.StoredProcedure);
                // var result = Dbcon.Execute(query, sp_param, commandType: CommandType.StoredProcedure);



                var procedure = "dbo.USP_APP_CHEOMSAK_LOGIN";
                //var values = new { in_UserName = "yooby", in_Pwd = "qwerty1234*" };
                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@in_UserName", "yooby");
                oParam.Add("@in_Pwd", "qwerty1234*");

                IEnumerable<T> results = Dbcon.Query<T>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();
                //results.ForEach(r => Console.WriteLine($"{r.OrderID} {r.Subtotal}"));
                Dbcon.Close();

                //if (results.Count() < 1)
                //    return false;

                return results;
            }
        }

        public List<Authenticate_loginModel> Get_api_authenticate_type02(IConfiguration configuration, string username, string password)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_APP_CHEOMSAK_LOGIN";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@in_UserName", username.ToString());
                oParam.Add("@in_Pwd", password.ToString());

                var resultlist = Dbcon.Query<Authenticate_loginModel>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();
                //results.ForEach(r => Console.WriteLine($"{r.OrderID} {r.Subtotal}"));
                Dbcon.Close();

                //if (results.Count() < 1)
                //    return false;

                return resultlist;
            }

        }
        #endregion


        #region 공통 모듈 


        #endregion

        #region 공통 모듈 (조회 조건 영역)  - 차시명
        public List<CommonModel_SelectBox_Cheomsak_LectureName> get_api_selectbox_cheomask_lecturename(int grade, int term, string subject, string course, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_LECTURENAME_SELECTED";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_GRADE", grade);
                oParam.Add("@IN_TERM", term);
                oParam.Add("@IN_SUBJECT", subject.ToString());
                oParam.Add("@IN_COURSE", course.ToString());
                var resultlist = Dbcon.Query<CommonModel_SelectBox_Cheomsak_LectureName>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 공통 모듈 (조회 조건 영역)  - 첨삭 채점자
        public List<D_MODEL_USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST> Api_Get_Edits_Cheomsak_Part_In_Teacher_List(P_MODEL_USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_PartTeacherID", p_model.IN_PartTeacherID.ToString());
                oParam.Add("@IN_AIYN", p_model.IN_AIYN.ToString());
                oParam.Add("@IN_AISearch", p_model.IN_AISearch.ToString());

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 공통 모듈 (조회 조건 영역)  - 공용코드
        public List<D_SelectBox_Common_Code> get_api_selectbox_common_code(string pcode, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_ADM_CODE_READ";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@in_CODE", pcode);
                var resultlist = Dbcon.Query<D_SelectBox_Common_Code>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }


        #endregion

    }
}
