﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Dapper;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;

using CRM_API_Cafe_NoBroand.Models;

namespace CRM_API_Cafe_NoBroand.Helpers
{

    public class AppGradingDataAccess
    {
        #region 밀크T 첨삭교사 - 채점리스트 요약
        public List<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO> Api_Get_Edits_Cheomsak_Result_List_Info(P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_RESULT_LIST_INFO_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_SDATE", p_model.IN_SDATE.ToString());
                oParam.Add("@IN_EDATE", p_model.IN_EDATE.ToString());
                oParam.Add("@IN_GRADE", p_model.IN_GRADE.ToString());
                oParam.Add("@IN_TERM", p_model.IN_TERM.ToString());
                oParam.Add("@IN_SEARCHTYPE", p_model.IN_SEARCHTYPE.ToString());
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_SUBJECT", p_model.IN_SUBJECT.ToString());
                oParam.Add("@IN_TSTATUS", p_model.IN_TSTATUS.ToString());
                oParam.Add("@IN_COURSE", p_model.IN_COURSE.ToString());
                oParam.Add("@IN_LECTURENAME", p_model.IN_LECTURENAME.ToString());
                oParam.Add("@IN_RESULTYN", p_model.IN_RESULTYN.ToString());
                oParam.Add("@IN_SENDYN", p_model.IN_SENDYN.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@IN_NEWYN", p_model.IN_NEWYN.ToString());

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 첨삭교사 - 채점리스트 상세
        public List<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST> Api_Get_Edits_Cheomsak_Result_List(P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_RESULT_LIST_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_SDATE", p_model.IN_SDATE.ToString());
                oParam.Add("@IN_EDATE", p_model.IN_EDATE.ToString());
                oParam.Add("@IN_GRADE", p_model.IN_GRADE.ToString());
                oParam.Add("@IN_TERM", p_model.IN_TERM.ToString());
                oParam.Add("@IN_SEARCHTYPE", p_model.IN_SEARCHTYPE.ToString());
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_SUBJECT", p_model.IN_SUBJECT.ToString());
                oParam.Add("@IN_TSTATUS", p_model.IN_TSTATUS.ToString());
                oParam.Add("@IN_COURSE", p_model.IN_COURSE.ToString());
                oParam.Add("@IN_LECTURENAME", p_model.IN_LECTURENAME.ToString());
                oParam.Add("@IN_RESULTYN", p_model.IN_RESULTYN.ToString());
                oParam.Add("@IN_SENDYN", p_model.IN_SENDYN.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@IN_NEWYN", p_model.IN_NEWYN.ToString());
                oParam.Add("@in_OrderByColName", p_model.IN_ORDERBYCOLNAME.ToString());
                oParam.Add("@in_OrderBy", p_model.IN_ORDERBY.ToString());
                oParam.Add("@in_PageNo", p_model.IN_PAGENO);
                oParam.Add("@in_PageSize", p_model.IN_PAGESIZE);

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 첨삭교사 파트장 - 채점리스트 요약
        public List<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO> Api_Get_Edits_Cheomsak_Result_List_Info_Part(P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO_PART p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_RESULT_LIST_INFO_PART_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_SDATE", p_model.IN_SDATE.ToString());
                oParam.Add("@IN_EDATE", p_model.IN_EDATE.ToString());
                oParam.Add("@IN_GRADE", p_model.IN_GRADE.ToString());
                oParam.Add("@IN_TERM", p_model.IN_TERM.ToString());
                oParam.Add("@IN_SEARCHTYPE", p_model.IN_SEARCHTYPE.ToString());
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_SUBJECT", p_model.IN_SUBJECT.ToString());
                oParam.Add("@IN_TSTATUS", p_model.IN_TSTATUS.ToString());
                oParam.Add("@IN_COURSE", p_model.IN_COURSE.ToString());
                oParam.Add("@IN_LECTURENAME", p_model.IN_LECTURENAME.ToString());
                oParam.Add("@IN_RESULTYN", p_model.IN_RESULTYN.ToString());
                oParam.Add("@IN_SENDYN", p_model.IN_SENDYN.ToString());
                oParam.Add("@IN_PARTTEACHERID", p_model.IN_PARTTEACHERID.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@IN_AIYN", p_model.IN_AIYN.ToString());
                oParam.Add("@IN_NEWYN", p_model.IN_NEWYN.ToString());

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 첨삭교사 파트장 - 채점리스트 상세
        public List<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST> Api_Get_Edits_Cheomsak_Result_List_Part(P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_PART p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_RESULT_LIST_PART_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_SDATE", p_model.IN_SDATE.ToString());
                oParam.Add("@IN_EDATE", p_model.IN_EDATE.ToString());
                oParam.Add("@IN_GRADE", p_model.IN_GRADE.ToString());
                oParam.Add("@IN_TERM", p_model.IN_TERM.ToString());
                oParam.Add("@IN_SEARCHTYPE", p_model.IN_SEARCHTYPE.ToString());
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_SUBJECT", p_model.IN_SUBJECT.ToString());
                oParam.Add("@IN_TSTATUS", p_model.IN_TSTATUS.ToString());
                oParam.Add("@IN_COURSE", p_model.IN_COURSE.ToString());
                oParam.Add("@IN_LECTURENAME", p_model.IN_LECTURENAME.ToString());
                oParam.Add("@IN_RESULTYN", p_model.IN_RESULTYN.ToString());
                oParam.Add("@IN_SENDYN", p_model.IN_SENDYN.ToString());
                oParam.Add("@IN_PARTTEACHERID", p_model.IN_PARTTEACHERID.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@IN_AIYN", p_model.IN_AIYN.ToString());
                oParam.Add("@IN_NEWYN", p_model.IN_NEWYN.ToString());
                oParam.Add("@in_OrderByColName", p_model.IN_ORDERBYCOLNAME.ToString());
                oParam.Add("@in_OrderBy", p_model.IN_ORDERBY.ToString());
                oParam.Add("@in_PageNo", p_model.IN_PAGENO);
                oParam.Add("@in_PageSize", p_model.IN_PAGESIZE);

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 첨삭 - 초기화
        public D_USP_EDITS_CHEOMSAK_TEACHER_RESET Api_Set_Edits_Cheomsak_Teacher_Reset(P_USP_EDITS_CHEOMSAK_TEACHER_RESET p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = p_model.IN_NEWYN == "Y" ? "dbo.USP_EDITS_CHEOMSAK_TEACHER_RESET_NEW" : "dbo.USP_EDITS_CHEOMSAK_TEACHER_RESET";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());                
                oParam.Add("@IN_QIDX", p_model.IN_QIDX.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());

                if(p_model.IN_NEWYN == "Y")
                {
                    oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                    oParam.Add("@IN_ResetReason", p_model.IN_ResetReason.ToString());
                    oParam.Add("@IN_ResetReasonEtc", p_model.IN_ResetReasonEtc.ToString());
                }
                else
                {
                    oParam.Add("@IN_MIDX", p_model.IN_MIDX.ToString());
                }

                var resultlist = Dbcon.Query<D_USP_EDITS_CHEOMSAK_TEACHER_RESET>(procedure, oParam, commandType: CommandType.StoredProcedure).First();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 요약 
        public D_MODEL_USP_EDITS_CHEOMSAK_RESULT_PART_COUNT Api_Edits_Cheomsak_Result_Management_Count_Part(P_MODEL_USP_EDITS_CHEOMSAK_RESULT_PART_COUNT p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_RESULT_PART_COUNT_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_SDATE", p_model.IN_SDATE.ToString());
                oParam.Add("@IN_EDATE", p_model.IN_EDATE.ToString());
                oParam.Add("@IN_GRADE", p_model.IN_GRADE.ToString());
                oParam.Add("@IN_TERM", p_model.IN_TERM.ToString());
                oParam.Add("@IN_SEARCHTYPE", p_model.IN_SEARCHTYPE.ToString());
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_SUBJECT", p_model.IN_SUBJECT.ToString());
                oParam.Add("@IN_TSTATUS", p_model.IN_TSTATUS.ToString());
                oParam.Add("@IN_COURSE", p_model.IN_COURSE.ToString());
                oParam.Add("@IN_LECTURENAME", p_model.IN_LECTURENAME.ToString());
                oParam.Add("@IN_RESULTYN", p_model.IN_RESULTYN.ToString());
                oParam.Add("@IN_SENDYN", p_model.IN_SENDYN.ToString());
                oParam.Add("@IN_PARTTEACHERID", p_model.IN_PARTTEACHERID.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@IN_POOR", p_model.IN_POOR.ToString());
                oParam.Add("@IN_EVAL", p_model.IN_EVAL.ToString());
                oParam.Add("@IN_AIYN", p_model.IN_AIYN.ToString());
                oParam.Add("@IN_NEWYN", p_model.IN_NEWYN.ToString());

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_PART_COUNT>(procedure, oParam, commandType: CommandType.StoredProcedure).First();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 채점리스트 
        public List<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART> Api_Get_Edits_Cheomsak_Result_Management_List_Part(P_MODEL_USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART_NEW";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_SDATE", p_model.IN_SDATE.ToString());
                oParam.Add("@IN_EDATE", p_model.IN_EDATE.ToString());
                oParam.Add("@IN_GRADE", p_model.IN_GRADE.ToString());
                oParam.Add("@IN_TERM", p_model.IN_TERM.ToString());
                oParam.Add("@IN_SEARCHTYPE", p_model.IN_SEARCHTYPE.ToString());
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_SUBJECT", p_model.IN_SUBJECT.ToString());
                oParam.Add("@IN_TSTATUS", p_model.IN_TSTATUS.ToString());
                oParam.Add("@IN_COURSE", p_model.IN_COURSE.ToString());
                oParam.Add("@IN_LECTURENAME", p_model.IN_LECTURENAME.ToString());
                oParam.Add("@IN_RESULTYN", p_model.IN_RESULTYN.ToString());
                oParam.Add("@IN_SENDYN", p_model.IN_SENDYN.ToString());
                oParam.Add("@IN_PARTTEACHERID", p_model.IN_PARTTEACHERID.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@IN_POOR", p_model.IN_POOR.ToString());
                oParam.Add("@IN_EVAL", p_model.IN_EVAL.ToString());
                oParam.Add("@IN_AIYN", p_model.IN_AIYN.ToString());
                oParam.Add("@IN_NEWYN", p_model.IN_NEWYN.ToString());
                oParam.Add("@in_OrderByColName", p_model.IN_ORDERBYCOLNAME.ToString());
                oParam.Add("@in_OrderBy", p_model.IN_ORDERBY.ToString());
                oParam.Add("@in_PageNo", p_model.IN_PAGENO);
                oParam.Add("@in_PageSize", p_model.IN_PAGESIZE);

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 불량첨삭 리스트 
        public List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_LIST> Api_Get_Edits_Cheomsak_Poor_List(P_MODEL_USP_EDITS_CHEOMSAK_POOR_LIST p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_POOR_LIST";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                oParam.Add("@IN_QIDX", p_model.IN_QIDX.ToString());

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_POOR_LIST>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 불량첨삭 저장 
        public List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT> Api_Set_Edits_Cheomsak_Poor_Insert(P_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_POOR_INSERT";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                oParam.Add("@IN_QIDX", p_model.IN_QIDX);
                oParam.Add("@IN_POORCONTENT", p_model.IN_POORCONTENT.ToString());
                oParam.Add("@IN_TEACHERID", p_model.IN_TEACHERID.ToString());
                oParam.Add("@OUT_Return", dbType: DbType.Int32,  direction: ParameterDirection.Output);

                Dbcon.Execute(procedure, oParam, commandType: CommandType.StoredProcedure);
                //Dbcon.Query(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();
                D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT tmp = new D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT();
                tmp.Out_ReturnCD = oParam.Get<Int32>("@OUT_Return").ToString();
                if (tmp.Out_ReturnCD == "1") {
                    tmp.Out_ReturnValue = "Success";
                }
                else {
                    tmp.Out_ReturnValue = "Fail";
                }
                tmp.StoredProcedure_Name = "USP_EDITS_CHEOMSAK_POOR_INSERT";
                List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT> lpf = new List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT>();
                lpf.Add(new D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT() { Out_ReturnCD = tmp.Out_ReturnCD, Out_ReturnValue = tmp.Out_ReturnValue, StoredProcedure_Name = tmp.StoredProcedure_Name });
                return lpf;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 불량첨삭 해지
        public List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_CLEAR> Api_Get_Edits_Cheomsak_Poor_Clear(P_MODEL_USP_EDITS_CHEOMSAK_POOR_CLEAR p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_POOR_CLEAR";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                oParam.Add("@IN_QIDX", p_model.IN_QIDX.ToString());

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_POOR_CLEAR>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 첨삭 가이드 
        public List<D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ> Api_Get_Edits_Cheomsak_Teacher_Guide_Read(P_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = "dbo.USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                oParam.Add("@IN_QIDX", p_model.IN_QIDX);

                var resultlist = Dbcon.Query<D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ>(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                return resultlist;
            }
        }
        #endregion

        #region 밀크T 채점관리 - 첨삭 가이드 고객 점수 저장
        public List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT> Api_Set_Edits_Cheomsak_Teacher_Guide_score(P_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_SCORE p_model, IConfiguration configuration)
        {
            using (SqlConnection Dbcon = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                var procedure = p_model.IN_NEWYN == "Y" ? "dbo.USP_EDITS_CHEOMSAK_TEACHER_GUIDE_SCORE_AI" : "dbo.USP_EDITS_CHEOMSAK_TEACHER_GUIDE_SCORE";

                DynamicParameters oParam = new DynamicParameters();
                oParam.Add("@IN_USERID", p_model.IN_USERID.ToString());
                oParam.Add("@IN_CCODE", p_model.IN_CCODE.ToString());
                oParam.Add("@IN_QIDX", p_model.IN_QIDX);
                oParam.Add("@IN_SCORE", p_model.IN_SCORE);
                oParam.Add("@OUT_Return", dbType: DbType.Int32, direction: ParameterDirection.Output);

                Dbcon.Execute(procedure, oParam, commandType: CommandType.StoredProcedure);
                //Dbcon.Query(procedure, oParam, commandType: CommandType.StoredProcedure).ToList();

                Dbcon.Close();

                var tmp = new D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT();
                tmp.Out_ReturnCD = oParam.Get<Int32>("@OUT_Return").ToString();
                if (tmp.Out_ReturnCD == "1")
                {
                    tmp.Out_ReturnValue = "Success";
                }
                else
                {
                    tmp.Out_ReturnValue = "Fail";
                }
                tmp.StoredProcedure_Name = procedure;
                var lpf = new List<D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT>();
                lpf.Add(new D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT() { Out_ReturnCD = tmp.Out_ReturnCD, Out_ReturnValue = tmp.Out_ReturnValue, StoredProcedure_Name = tmp.StoredProcedure_Name });
                return lpf;
            }
        }
        #endregion

    }
}
