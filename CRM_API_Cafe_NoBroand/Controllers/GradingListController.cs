﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Authorization;
using CRM_API_Cafe_NoBroand.Models;
using CRM_API_Cafe_NoBroand.Helpers;

using Microsoft.Extensions.Configuration;

namespace CRM_API_Cafe_NoBroand.Controllers
{
    [Authorize]
    [Route("milkt_teaching/[controller]")]
    [ApiController]
    public class GradingListController : ControllerBase
    {
        AppGradingDataAccess AGD_Acc = new AppGradingDataAccess();

        #region 밀크T 첨삭교사 - 채점리스트 요약                 Api_Edits_Cheomsak_Result_List_Info
        /// <summary>
        /// 밀크T 첨삭교사 - 채점리스트 요악 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Result_List_Info
        ///     {
        /// 
        ///         "iN_SDATE": "2019-06-16",
        ///         "iN_EDATE": "2019-06-16",
        ///         "iN_GRADE": "",
        ///         "iN_TERM": "",
        ///         "iN_SEARCHTYPE": "ID",
        ///         "iN_USERID": "",
        ///         "iN_SUBJECT": "",
        ///         "iN_TSTATUS": "",
        ///         "iN_COURSE": "",
        ///         "iN_LECTURENAME": "",
        ///         "iN_RESULTYN": "S",
        ///         "iN_SENDYN": "",
        ///         "iN_TEACHERID": "kimjs"
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Result_List_Info")]
        public IActionResult Api_Edits_Cheomsak_Result_List_Info([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Result_List_Info(model, configuration);   //.get_api_selectbox_cheomask_lecturename(model.Grade, model.Term, model.Subject, model.Course, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Result_List_Info" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 첨삭교사 - 채점리스트 상세                 Api_Edits_Cheomsak_Result_List
        /// <summary>
        /// 밀크T 첨삭교사 - 채점리스트 상세 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Result_List
        ///     {
        /// 
        ///         "iN_SDATE": "2019-06-16",
        ///         "iN_EDATE": "2019-06-16",
        ///         "iN_GRADE": "",
        ///         "iN_TERM": "",
        ///         "iN_SEARCHTYPE": "ID",
        ///         "iN_USERID": "",
        ///         "iN_SUBJECT": "",
        ///         "iN_TSTATUS": "",
        ///         "iN_COURSE": "",
        ///         "iN_LECTURENAME": "",
        ///         "iN_RESULTYN": "S",
        ///         "iN_SENDYN": "",
        ///         "iN_TEACHERID": "kimjs",
        ///         "in_OrderByColName":"RegDate",
        ///         "in_OrderBy":"DESC",
        ///         "in_PageNo":1,
        ///         "in_PageSize":120
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Result_List")]
        public IActionResult Api_Edits_Cheomsak_Result_List([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Result_List(model, configuration);   //.get_api_selectbox_cheomask_lecturename(model.Grade, model.Term, model.Subject, model.Course, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Result_List" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 첨삭 파트장교사 - 채점리스트 요약               Api_Edits_Cheomsak_Result_List_Info_Part
        /// <summary>
        /// 밀크T 첨삭 파트장교사 - 채점리스트 요악 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Result_List_Info_Part
        ///     {
        /// 
        ///         "iN_SDATE": "2019-06-16",
        ///         "iN_EDATE": "2019-06-16",
        ///         "iN_GRADE": "",
        ///         "iN_TERM": "",
        ///         "iN_SEARCHTYPE": "ID",
        ///         "iN_USERID": "",
        ///         "iN_SUBJECT": "",
        ///         "iN_TSTATUS": "",
        ///         "iN_COURSE": "",
        ///         "iN_LECTURENAME": "",
        ///         "iN_RESULTYN": "S",
        ///         "iN_SENDYN": "",
        ///         "IN_PARTTEACHERID": "artking",
        ///         "iN_TEACHERID": ""
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Result_List_Info_Part")]
        public IActionResult Api_Edits_Cheomsak_Result_List_Info_Part([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO_PART model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Result_List_Info_Part(model, configuration);   //.get_api_selectbox_cheomask_lecturename(model.Grade, model.Term, model.Subject, model.Course, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Result_List_Info_Part" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 첨삭 파트장교사 - 채점리스트 상세               Api_Edits_Cheomsak_Result_List_Part
        /// <summary>
        /// 밀크T 파트장교사 - 채점리스트 상세 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Result_List_Part
        ///     {
        /// 
        ///         "iN_SDATE": "2018-08-04'",
        ///         "iN_EDATE": "2018-09-04",
        ///         "iN_GRADE": "",
        ///         "iN_TERM": "",
        ///         "iN_SEARCHTYPE": "",
        ///         "iN_USERID": "",
        ///         "iN_SUBJECT": "",
        ///         "iN_TSTATUS": "",
        ///         "iN_COURSE": "",
        ///         "iN_LECTURENAME": "",
        ///         "iN_RESULTYN": "",
        ///         "iN_SENDYN": "",
        ///         "IN_PARTTEACHERID": "yooby",
        ///         "iN_TEACHERID": "",
        ///         "in_OrderByColName":"RegDate",
        ///         "in_OrderBy":"DESC",
        ///         "in_PageNo":1,
        ///         "in_PageSize":120
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Result_List_Part")]
        public IActionResult Api_Edits_Cheomsak_Result_List_Part([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_PART model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Result_List_Part(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Result_List_Part" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 첨삭 - 초기화
        /// <summary>
        /// 밀크T 첨삭 - 초기화
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_EDITS_CHEOMSAK_TEACHER_RESET
        ///     {
        /// 
        ///         "IN_USERID": "KOO3",
        ///         "IN_MIDX": "95",
        ///         "IN_QIDX": "0",
        ///         "IN_TEACHERID": "hbsking"
        ///     }
        /// </remarks>
        [HttpPost("Api_EDITS_CHEOMSAK_TEACHER_RESET")]
        public IActionResult Api_EDITS_CHEOMSAK_TEACHER_RESET([FromBody]P_USP_EDITS_CHEOMSAK_TEACHER_RESET model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Set_Edits_Cheomsak_Teacher_Reset(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Set_Edits_Cheomsak_Teacher_Reset" });

            return Ok(str_list);

        }
        #endregion
    }
}