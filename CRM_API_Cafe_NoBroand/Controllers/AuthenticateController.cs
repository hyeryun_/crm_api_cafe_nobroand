﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using CRM_API_Cafe_NoBroand.Services;
using CRM_API_Cafe_NoBroand.Models;

using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CRM_API_Cafe_NoBroand.Controllers
{
    [Authorize]
    [ApiController]
    [Route("milkt_teaching/[controller]")]
    public class AuthenticateController : ControllerBase
    {
        private IAuthenticateService _AuthenticateService;

        public AuthenticateController(IAuthenticateService authenticateService)
        {
            _AuthenticateService = authenticateService;
        }


        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model, [FromServices]IConfiguration configuration)
        {
            var user = _AuthenticateService.Authenticate(model.Username, model.Password, configuration);

            if (user == null)
                return BadRequest(new { fail = "아이디 또는 비밀번호를 확인해주세요." });

            return Ok(user);
        }

        
        [HttpPost("cheomsaklecturename")]
        public IActionResult Cheomsaklecturename()
        {
            return Ok("TEST2");
        }

    }
}
