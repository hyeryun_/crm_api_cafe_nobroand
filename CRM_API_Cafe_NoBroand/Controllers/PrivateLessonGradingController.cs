﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Authorization;
using CRM_API_Cafe_NoBroand.Models;
using CRM_API_Cafe_NoBroand.Helpers;

using Microsoft.Extensions.Configuration;

namespace CRM_API_Cafe_NoBroand.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrivateLessonGradingController : ControllerBase
    {
        AppPrivateLessonGradingDataAccess APLGD_Acc = new AppPrivateLessonGradingDataAccess();

        #region 밀크T 첨삭 교사 채점 화면  - 상태                Api_Set_Edits_Cheomsak_Status
        [AllowAnonymous]
        [HttpPost("Api_PrivateLessonGrading_Status")]
        public IActionResult Api_PrivateLessonGrading_Status([FromBody] P_MODEL_USP_EDITS_CHEOMSAK_STATUS model, [FromServices] IConfiguration configuration)
        {
            APLGD_Acc.Api_Set_Edits_Cheomsak_Status(model, configuration);

            return Ok();
        }


        #endregion


        #region 밀크T 첨삭 교사 채점 화면  - 문제                Api_PrivateLessonGrading_QuestionInfo
        [AllowAnonymous]
        [HttpPost("Api_PrivateLessonGrading_QuestionInfo")]
        public IActionResult Api_PrivateLessonGrading_QuestionInfo([FromBody]P_MODEL_Global_PrivateLessonGrading_type01 model, [FromServices] IConfiguration configuration)
        {
            var str_list = APLGD_Acc.Api_Get_Edits_Cheomsak_Lecture_Question_Info(model, configuration);

            return Ok(str_list);
        }


        #endregion

        #region 
        [AllowAnonymous]
        [HttpPost("Api_PrivateLessonGrading_Eval")]
        public IActionResult Api_PrivateLessonGrading_Eval([FromBody] P_MODEL_USP_EDITS_CHEOMSAK_PARTTEACHER_EVAL model, [FromServices] IConfiguration configuration)
        {
            var intReturn = APLGD_Acc.Api_Get_Edits_Cheomsak_PartTeacher_Eval(model, configuration);

            return Ok(new { result = intReturn });
        }
        #endregion




    }
}