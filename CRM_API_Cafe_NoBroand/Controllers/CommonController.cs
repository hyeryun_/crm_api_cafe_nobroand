﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Authorization;
using CRM_API_Cafe_NoBroand.Models;
using CRM_API_Cafe_NoBroand.Helpers;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace CRM_API_Cafe_NoBroand.Controllers
{
    [Authorize]
    [Route("milkt_teaching/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        AppCommonDataAccess ACD_Acc = new AppCommonDataAccess();

        [HttpPost("cheomsak_lecturename")]
        public IActionResult selectbox_cheomask_lecturename([FromBody]FromModel_SelectBox_Cheomsak_LectureName model, [FromServices]IConfiguration configuration)
        {
            var str_list = ACD_Acc.get_api_selectbox_cheomask_lecturename(model.Grade, model.Term, model.Subject, model.Course, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. selectbox_cheomask_lecturename" });

            return Ok(str_list);

        }



        [HttpGet]
        public IActionResult cheomsak_lecturename()
        {
            return Ok("TEST2");
        }


        #region 공통모듈(조회조건) - 공통코드                common_code
        /// <summary>
        /// 공통모듈(조회조건) - 첨삭 채점자 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /common_code
        ///     {
        /// 
        ///         "in_CODE": "SEMESTERBOOK_REQUEST_REASON"
        ///     }
        /// </remarks>
        [HttpPost("common_code")]
        public IActionResult selectbox_common_code([FromBody]P_SelectBox_Common_Code model, [FromServices]IConfiguration configuration)
        {
            var str_list = ACD_Acc.get_api_selectbox_common_code(model.PCODE, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. selectbox_common_code" });

            return Ok(str_list);

        }
        #endregion

        #region 공통모듈(조회조건) - 첨삭 채점자                Api_Edits_Cheomsak_Part_In_Teacher_List
        /// <summary>
        /// 공통모듈(조회조건) - 첨삭 채점자 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Part_In_Teacher_List
        ///     {
        /// 
        ///         "IN_PartTeacherID": "partking"
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Part_In_Teacher_List")]
        public IActionResult Api_Edits_Cheomsak_Part_In_Teacher_List([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST model, [FromServices]IConfiguration configuration)
        {
            var str_list = ACD_Acc.Api_Get_Edits_Cheomsak_Part_In_Teacher_List(model, configuration);
            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Part_In_Teacher_List" });

            return Ok(str_list);
        }
        #endregion




    }
}