﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Authorization;
using CRM_API_Cafe_NoBroand.Models;
using CRM_API_Cafe_NoBroand.Helpers;

using Microsoft.Extensions.Configuration;

namespace CRM_API_Cafe_NoBroand.Controllers
{
    [Authorize]
    [Route("milkt_teaching/[controller]")]
    [ApiController]
    public class GradingMngController : ControllerBase
    {
        AppGradingDataAccess AGD_Acc = new AppGradingDataAccess();

        #region 밀크T 채점관리 -  요약               Api_Get_Edits_Cheomsak_Eval_Count_Part
        /// <summary>
        /// 밀크T 채점관리 -  요약
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Get_Edits_Cheomsak_Eval_Count_Part
        ///     {
        /// 
        ///         "iN_SDATE": "2018-08-04'",
        ///         "iN_EDATE": "2018-09-04",
        ///         "iN_GRADE": "",
        ///         "iN_TERM": "",
        ///         "iN_SEARCHTYPE": "ID",
        ///         "iN_USERID": "",
        ///         "iN_SUBJECT": "",
        ///         "iN_TSTATUS": "",
        ///         "iN_COURSE": "",
        ///         "iN_LECTURENAME": "",
        ///         "iN_RESULTYN": "",
        ///         "iN_SENDYN": "",
        ///         "IN_PARTTEACHERID": "yooby",
        ///         "IN_TEACHERID": "",
        ///         "IN_POOR": "",
        ///         "IN_EVAL": ""
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Result_Management_Count_Part")]
        public IActionResult Api_Edits_Cheomsak_Result_Management_Count_Part([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_RESULT_PART_COUNT model, [FromServices] IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Edits_Cheomsak_Result_Management_Count_Part(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Result_Management_Count_Part" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 채점관리 -  리스트 상세               Api_Edits_Cheomsak_Result_Management_List_Part
        /// <summary>
        /// 밀크T 채점관리 -  리스트 상세
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Result_Management_List_Part
        ///     {
        /// 
        ///         "iN_SDATE": "2018-08-04'",
        ///         "iN_EDATE": "2018-09-04",
        ///         "iN_GRADE": "",
        ///         "iN_TERM": "",
        ///         "iN_SEARCHTYPE": "ID",
        ///         "iN_USERID": "",
        ///         "iN_SUBJECT": "",
        ///         "iN_TSTATUS": "",
        ///         "iN_COURSE": "",
        ///         "iN_LECTURENAME": "",
        ///         "iN_RESULTYN": "",
        ///         "iN_SENDYN": "",
        ///         "IN_PARTTEACHERID": "yooby",
        ///         "IN_TEACHERID": "",
        ///         "IN_POOR": "",
        ///         "IN_EVAL": "",
        ///         "IN_ORDERBYCOLNAME":"Mark",
        ///         "IN_ORDERBY":"DESC",
        ///         "IN_PAGENO":1,
        ///         "IN_PAGESIZE":20
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_Cheomsak_Result_Management_List_Part")]
        public IActionResult Api_Edits_Cheomsak_Result_Management_List_Part([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Result_Management_List_Part(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Result_Management_List_Part" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 채점관리 -  불량첨삭 리스트               Api_Edits_cheomsak_Poor_List
        /// <summary>
        /// 밀크T 채점관리 -  불량첨삭 리스트 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_cheomsak_Poor_List
        ///     {
        /// 
        ///         "IN_USERID": "E20021200010",
        ///         "IN_CCODE": "CT9M62180002",
        ///         "IN_QIDX": 157
        ///     }
        /// </remarks>
        [HttpPost("Api_Edits_cheomsak_Poor_List")]
        public IActionResult Api_Edits_cheomsak_Poor_List([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_POOR_LIST model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Poor_List(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_cheomsak_Poor_List" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 채점관리 -  불량첨삭 저장               Api_Edits_cheomsak_Poor_Save
        /// <summary>
        /// 밀크T 채점관리 -  불량첨삭 저장 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_cheomsak_Poor_Save
        ///     {
        /// 
        ///         "IN_USERID": "hyjo4",
        ///         "IN_CCODE": "CT9K52180002",
        ///         "IN_QIDX": 10098,
        ///         "IN_POORCONTENT": "불량 내용 저장 테스트123",
        ///         "IN_TEACHERID": "partking"
        ///         
        ///     }
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("Api_Edits_cheomsak_Poor_Save")]
        public IActionResult Api_Edits_cheomsak_Poor_Save([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Set_Edits_Cheomsak_Poor_Insert(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_cheomsak_Poor_Save" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 채점관리 -  불량첨삭 해지               Api_Edits_cheomsak_Poor_Clear
        /// <summary>
        /// 밀크T 채점관리 -  불량첨삭 저장 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_cheomsak_Poor_Clear
        ///     {
        /// 
        ///         "IN_USERID": "koo5",
        ///         "IN_CCODE": "CT9S52180004",
        ///         "IN_QIDX": 10225,
        ///         
        ///     }
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("Api_Edits_cheomsak_Poor_Clear")]
        public IActionResult Api_Edits_cheomsak_Poor_Clear([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_POOR_CLEAR model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Poor_Clear(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_cheomsak_Poor_Clear" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 채점관리 -  첨삭 가이드                  Api_Edits_Cheomsak_Teacher_Guide_Read
        /// <summary>
        /// 밀크T 채점관리 -  첨삭 가이드 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_Cheomsak_Teacher_Guide_Read
        ///     {
        /// 
        ///         "IN_USERID": "koo5",
        ///         "IN_CCODE": "CT9M5218000101",
        ///         "IN_QIDX": 124
        ///     }
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("Api_Edits_Cheomsak_Teacher_Guide_Read")]
        public IActionResult Api_Edits_Cheomsak_Teacher_Guide_Read([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Get_Edits_Cheomsak_Teacher_Guide_Read(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_Cheomsak_Teacher_Guide_Read" });

            return Ok(str_list);

        }
        #endregion

        #region 밀크T 채점관리 -  첨삭 가이드 고객점수 저장               Api_Edits_cheomsak_Teacher_Guide_score
        /// <summary>
        /// 밀크T 채점관리 -  첨삭 가이드 고객 점수 저장 
        /// </summary>
        /// <remarks>
        /// 
        /// Sample response :
        /// 
        ///     POST  /Api_Edits_cheomsak_Teacher_Guide_score
        ///     {
        /// 
        ///         "IN_USERID": "koo5",
        ///         "IN_CCODE": "CT9K52180001",
        ///         "IN_QIDX": 124,
        ///         "IN_SCORE": 90
        ///         
        ///     }
        /// </remarks>
        [AllowAnonymous]
        [HttpPost("Api_Edits_cheomsak_Teacher_Guide_score")]
        public IActionResult Api_Edits_cheomsak_Teacher_Guide_score([FromBody]P_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_SCORE model, [FromServices]IConfiguration configuration)
        {
            var str_list = AGD_Acc.Api_Set_Edits_Cheomsak_Teacher_Guide_score(model, configuration);

            if (str_list == null)
                return BadRequest(new { message = "err. Api_Edits_cheomsak_Teacher_Guide_score" });

            return Ok(str_list);

        }
        #endregion

        

    }
}
