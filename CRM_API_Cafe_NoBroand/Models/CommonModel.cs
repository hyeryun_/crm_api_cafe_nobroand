﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRM_API_Cafe_NoBroand.Models
{
    public class FromModel_SelectBox_Cheomsak_LectureName
    {
        public int Grade { get; set; }
        public int Term { get; set; }

        public string Subject { get; set; }

        public string Course { get; set; }
    }

    public class CommonModel_SelectBox_Cheomsak_LectureName
    {
        public string cCode { get; set; }
        public string Title { get; set; }
    }

    #region   조회조건: 차시명
    public class P_SelectBox_Common_Code
    {
        public string PCODE { get; set; }
    }

    public class D_SelectBox_Common_Code
    {
        public string C_CODE { get; set; }
        public string C_PARENT_CODE { get; set; }
        public string C_VALUE { get; set; }
        public string C_DESC { get; set; }
        public string C_USE_YN { get; set; }
        public string C_ORDERBY { get; set; }
        public string CodeRegDate { get; set; }
        public string CodeRegID { get; set; }
    }
    #endregion

    public class Return_Code_fail
    {
        public string CD_fail { get; set; }
        public string CD_value { get; set; }
    }


    #region   조회조건: 첨삭 채점자 
    public class P_MODEL_USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST
    {
        public string IN_PartTeacherID { get; set; }
        public string IN_AIYN { get; set; }
        public string IN_AISearch { get; set; }
    }
    public class D_MODEL_USP_EDITS_CHEOMSAK_PART_IN_TEACHER_LIST
    {
        public string TeacherID { get; set; }
        public string TeacherName { get; set; }
    }
    #endregion

    #region   공통 영역:  Insert / Updte / Delete 에서 return 값 만 있는 경우 
    public class D_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT
    {
        public string StoredProcedure_Name { get; set; }
        public string Out_ReturnCD { get; set; }
        public string Out_ReturnValue { get; set; }
    }
    #endregion





}
