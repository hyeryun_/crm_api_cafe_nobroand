﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Security.Policy;

namespace CRM_API_Cafe_NoBroand.Models
{
    public class GradingModel
    {
    }

    #region 밀크T 첨삭교사 - 채점리스트 요약
    public class P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO
    {
        /// <summary>
        /// 시작일 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_SDATE { get; set; }
        /// <summary>
        /// 종료일 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_EDATE { get; set; }
        public string IN_GRADE { get; set; }
        public string IN_TERM { get; set; }
        /// <summary>
        /// 검색타입 
        /// </summary>
        /// <example>ID</example>
        [Required]
        public string IN_SEARCHTYPE { get; set; }
        public string IN_USERID { get; set; }
        public string IN_SUBJECT { get; set; }
        public string IN_TSTATUS { get; set; }
        public string IN_COURSE { get; set; }
        public string IN_LECTURENAME { get; set; }
        /// <summary>
        ///  
        /// </summary>
        /// <example>s</example>
        public string IN_RESULTYN { get; set; }
        public string IN_SENDYN { get; set; }
        /// <summary>
        /// 검색교사 
        /// </summary>
        /// <example>kimjs</example>
        [Required]
        public string IN_TEACHERID { get; set; }

        public string IN_NEWYN { get; set; }
    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO
    {
        /// <summary>
        /// 학습생 구분
        /// </summary>
        public string TStatusVal { get; set; }
        /// <summary>
        /// 전체(명)
        /// </summary>
        public int TotCnt { get; set; }
        /// <summary>
        /// 채점완료
        /// </summary>
        public int RsA { get; set; }
        /// <summary>
        /// 진행
        /// </summary>
        public int RsB { get; set; }
        /// <summary>
        /// 대기
        /// </summary>
        public int RsC { get; set; }
        /// <summary>
        /// 불량
        /// </summary>
        public int RsD { get; set; }
        /// <summary>
        /// 전송완료
        /// </summary>
        public int SendY { get; set; }
        /// <summary>
        /// 전송대기
        /// </summary>
        public int SendN { get; set; }
        /// <summary>
        /// 평균점수
        /// </summary>
        public float Score { get; set; }

    }


    public class P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST
    {
        public string IN_SDATE { get; set; }
        public string IN_EDATE { get; set; }
        public string IN_GRADE { get; set; }
        public string IN_TERM { get; set; }
        public string IN_SEARCHTYPE { get; set; }
        public string IN_USERID { get; set; }
        public string IN_SUBJECT { get; set; }
        public string IN_TSTATUS { get; set; }
        public string IN_COURSE { get; set; }
        public string IN_LECTURENAME { get; set; }
        public string IN_RESULTYN { get; set; }
        public string IN_SENDYN { get; set; }
        public string IN_TEACHERID { get; set; }
        public string IN_NEWYN { get; set; }
        public string IN_ORDERBYCOLNAME { get; set; }
        public string IN_ORDERBY { get; set; }
        public int IN_PAGENO { get; set; }
        public int IN_PAGESIZE { get; set; }

    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST
    {
        public int RowNum { get; set; }
        public int TotCnt { get; set; }
        public string TStatusVal { get; set; }
        public string RegDate { get; set; }
        public int Grade { get; set; }
        public int Term { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string C_VALUE { get; set; }
        public string Title { get; set; }
        public string Mode { get; set; }
        public string Mark { get; set; }
        public string Status { get; set; }
        public int score { get; set; }
        public string TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string PoorYN { get; set; }
        public int mIdx { get; set; }
        public int qIdx { get; set; }
        public string cCode { get; set; }
        public int Dur { get; set; }
        public int AIYN { get; set; }

    }
    #endregion

    #region 밀크T 첨삭교사 - 채점리스트 요약 파트장
    public class P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_INFO_PART
    {
        /// <summary>
        /// 시작일 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_SDATE { get; set; }
        /// <summary>
        /// 종료일 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_EDATE { get; set; }
        public string IN_GRADE { get; set; }
        public string IN_TERM { get; set; }
        /// <summary>
        /// 검색타입 
        /// </summary>
        /// <example>ID</example>
        [Required]
        public string IN_SEARCHTYPE { get; set; }
        public string IN_USERID { get; set; }
        public string IN_SUBJECT { get; set; }
        public string IN_TSTATUS { get; set; }
        public string IN_COURSE { get; set; }
        public string IN_LECTURENAME { get; set; }
        /// <summary>
        ///  
        /// </summary>
        /// <example>s</example>
        public string IN_RESULTYN { get; set; }
        public string IN_SENDYN { get; set; }
        /// <summary>
        /// 파트장  
        /// </summary>
        /// <example>kimjs</example>
        [Required]
        public string IN_PARTTEACHERID { get; set; }
        /// <summary>
        /// 검색교사 
        /// </summary>
        /// <example>kimjs</example>
        public string IN_TEACHERID { get; set; }

        public string IN_AIYN { get; set; }
        public string IN_NEWYN { get; set; }

    }

    public class P_MODEL_USP_EDITS_CHEOMSAK_RESULT_LIST_PART
    {
        public string IN_SDATE { get; set; }
        public string IN_EDATE { get; set; }
        public string IN_GRADE { get; set; }
        public string IN_TERM { get; set; }
        public string IN_SEARCHTYPE { get; set; }
        public string IN_USERID { get; set; }
        public string IN_SUBJECT { get; set; }
        public string IN_TSTATUS { get; set; }
        public string IN_COURSE { get; set; }
        public string IN_LECTURENAME { get; set; }
        public string IN_RESULTYN { get; set; }
        public string IN_SENDYN { get; set; }
        public string IN_PARTTEACHERID { get; set; }
        public string IN_TEACHERID { get; set; }
        public string IN_AIYN { get; set; }
        public string IN_NEWYN { get; set; }
        public string IN_ORDERBYCOLNAME { get; set; }
        public string IN_ORDERBY { get; set; }
        public int IN_PAGENO { get; set; }
        public int IN_PAGESIZE { get; set; }

    }

    #endregion

    #region 밀크T 첨삭 - 초기화
    public class P_USP_EDITS_CHEOMSAK_TEACHER_RESET
    {
        public string IN_USERID { get; set; }
        public Int64 IN_MIDX { get; set; }
        public Int64 IN_QIDX { get; set; }
        public string IN_TEACHERID { get; set; }
        public string IN_CCODE { get; set; }
        public string IN_ResetReason { get; set; }
        public string IN_ResetReasonEtc { get; set; }
        public string IN_NEWYN { get; set; }
    }

    public class D_USP_EDITS_CHEOMSAK_TEACHER_RESET
    {
        public string RESULT { get; set; }
    }
    #endregion

    #region 밀크T 채점관리 - 첨삭 제출정보 리스트
    public class P_MODEL_USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART
    {
        /// <summary>
        /// 시작일 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_SDATE { get; set; }
        /// <summary>
        /// 종료일 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_EDATE { get; set; }
        public string IN_GRADE { get; set; }
        public string IN_TERM { get; set; }
        /// <summary>
        /// 검색타입 
        /// </summary>
        /// <example>ID</example>
        [Required]
        public string IN_SEARCHTYPE { get; set; }
        public string IN_USERID { get; set; }
        public string IN_SUBJECT { get; set; }
        public string IN_TSTATUS { get; set; }
        public string IN_COURSE { get; set; }
        public string IN_LECTURENAME { get; set; }
        /// <summary>
        ///  
        /// </summary>
        /// <example>s</example>
        public string IN_RESULTYN { get; set; }
        public string IN_SENDYN { get; set; }
        /// <summary>
        /// 파트장  
        /// </summary>
        /// <example>kimjs</example>        
        public string IN_PARTTEACHERID { get; set; }
        /// <summary>
        /// 검색교사 
        /// </summary>
        /// <example>kimjs</example>
        public string IN_TEACHERID { get; set; }
        public string IN_POOR { get; set; }
        public string IN_EVAL { get; set; }
        public string IN_AIYN { get; set; }
        public string IN_NEWYN { get; set; }
        public string IN_ORDERBYCOLNAME { get; set; }
        public string IN_ORDERBY { get; set; }
        public int IN_PAGENO { get; set; }
        public int IN_PAGESIZE { get; set; }        
    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_RESULT_MANAGEMENT_LIST_PART
    {
        public int RowNum { get; set; }
        public int TotCnt { get; set; }
        public string TStatusVal { get; set; }
        public string RegDate { get; set; }
        public int Grade { get; set; }
        public int Term { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string C_VALUE { get; set; }
        public string Title { get; set; }
        public string Mode { get; set; }
        public string Mark { get; set; }
        public string Status { get; set; }
        public int score { get; set; }
        public string TeacherID { get; set; }
        public string TeacherName { get; set; }
        public string PoorYN { get; set; }
        public string EvalYN { get; set; }
        public string EvalCnt { get; set; }
        public int mIdx { get; set; }
        public int qIdx { get; set; }
        public string cCode { get; set; }
        public int Dur { get; set; }
        public int Aiyn { get; set; }

    }

    #endregion

    #region 밀크T 채점관리 - 첨삭 제출정보 요약

    public class P_MODEL_USP_EDITS_CHEOMSAK_RESULT_PART_COUNT
    {
        /// <summary>
        /// 시작일
        /// </summary>
        /// <example>2020-06-01</example>
        [Required]
        public string IN_SDATE { get; set; }

        /// <summary>
        /// 종료일 
        /// </summary>
        /// <example>2020-06-30</example>
        [Required]
        public string IN_EDATE { get; set; }

        /// <summary>
        /// 학년
        /// </summary>
        /// <example></example>
        public string IN_GRADE { get; set; }

        /// <summary>
        /// 학기 
        /// </summary>
        /// <example></example>
        public string IN_TERM { get; set; }

        /// <summary>
        /// 검색조건 
        /// </summary>
        /// <example>ID</example>
        public string IN_SEARCHTYPE { get; set; }

        /// <summary>
        /// 학생 아이디 
        /// </summary>
        /// <example></example>
        public string IN_USERID { get; set; }

        /// <summary>
        /// 과목
        /// </summary>
        /// <example></example>
        public string IN_SUBJECT { get; set; }

        /// <summary>
        /// 학습생 종류 
        /// </summary>
        /// <example></example>
        public string IN_TSTATUS { get; set; }

        /// <summary>
        /// 교과과정 
        /// </summary>
        /// <example></example>
        public string IN_COURSE { get; set; }

        /// <summary>
        /// 차시명
        /// </summary>
        /// <example></example>
        public string IN_LECTURENAME { get; set; }

        /// <summary>
        /// 채점여부 
        /// </summary>
        /// <example></example>
        public string IN_RESULTYN { get; set; }

        /// <summary>
        /// 전송여부 
        /// </summary>
        /// <example></example>
        public string IN_SENDYN { get; set; }

        /// <summary>
        /// 첨삭 채점자 
        /// </summary>
        /// <example>yooby</example>
        public string IN_PARTTEACHERID { get; set; }

        /// <summary>
        /// 로그인정보 
        /// </summary>
        /// <example></example>
        public string IN_TEACHERID { get; set; }

        /// <summary>
        /// 불량작성
        /// </summary>
        /// <example></example>
        public string IN_POOR { get; set; }

        /// <summary>
        /// 평가완료
        /// </summary>
        /// <example></example>
        public string IN_EVAL { get; set; }

        /// <summary>
        /// AI여부
        /// </summary>
        /// <example></example>
        public string IN_AIYN { get; set; }

        /// <summary>
        /// 신첨삭/구첨삭
        /// </summary>
        /// <example></example>
        public string IN_NEWYN { get; set; }
    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_RESULT_PART_COUNT
    {
        public Int64 TotCnt { get; set; }
        public Int64 EvalY { get; set; }
        public Int64 EvalN { get; set; }
        public double EvalPer { get; set; }
        public double GoalCnt { get; set; }

    }

    #endregion

    #region 밀크T 채점관리 - 불량첨삭 이력 리스트
    public class P_MODEL_USP_EDITS_CHEOMSAK_POOR_LIST
    {
        /// <summary>
        /// 학생 아이디 
        /// </summary>
        /// <example>koo5</example>
        [Required]
        public string IN_USERID { get; set; }
        /// <summary>
        /// 첨삭 코드 
        /// </summary>
        /// <example>2019-05-01</example>
        [Required]
        public string IN_CCODE { get; set; }

        [Required]
        public Int64 IN_QIDX { get; set; }

    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_POOR_LIST
    {
        public object UserID { get; set; }
        public int mIdx { get; set; }
        public int qIdx { get; set; }
        public string Poor_Content { get; set; }
        public string PartTeacherID { get; set; }
        public string RegDate { get; set; }

    }

    #endregion

    #region 밀크T 채점관리 - 불량첨삭 저장
    public class P_MODEL_USP_EDITS_CHEOMSAK_POOR_INSERT
    {
        /// <summary>
        /// 학생 아이디 
        /// </summary>
        /// <example>hyjo4</example>
        [Required]
        public string IN_USERID { get; set; }
        /// <summary>
        /// 첨삭 코드 
        /// </summary>
        /// <example>CT9K52180002</example>
        [Required]
        public string IN_CCODE { get; set; }
        /// <summary>
        ///  코드값
        /// </summary>
        /// <example>10098</example>
        [Required]
        public Int64 IN_QIDX { get; set; }
        /// <summary>
        /// 불량첨삭 내용
        /// </summary>
        /// <example>하하하</example>
        [Required]
        public string IN_POORCONTENT { get; set; }
        /// <summary>
        /// 등록자
        /// </summary>
        /// <example>safhks</example>
        [Required]
        public string IN_TEACHERID { get; set; }

    }

    #endregion

    #region 밀크T 채점관리 - 불량첨삭 해지
    public class P_MODEL_USP_EDITS_CHEOMSAK_POOR_CLEAR
    {
        /// <summary>
        /// 학생 아이디 
        /// </summary>
        /// <example>koo5</example>
        [Required]
        public string IN_USERID { get; set; }
        /// <summary>
        /// 첨삭 코드 
        /// </summary>
        /// <example>CT9S52180004</example>
        [Required]
        public string IN_CCODE { get; set; }
        /// <summary>
        ///  코드값
        /// </summary>
        /// <example>10225</example>
        [Required]
        public Int64 IN_QIDX { get; set; }

    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_POOR_CLEAR
    {
        /// <summary>
        /// 결과값 
        /// </summary>
        /// <example>Y</example>
        public string RESULT { get; set; }
        /// <summary>
        /// ???? 
        /// </summary>
        /// <example></example>
        public string PoorCnt { get; set; }

    }

    #endregion

    #region 밀크T 채점관리 - 첨삭가이드
    public class P_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ
    {
        /// <summary>
        /// 학생 아이디 
        /// </summary>
        /// <example>koo5</example>
        [Required]
        public string IN_USERID { get; set; }
        /// <summary>
        /// 첨삭 코드 
        /// </summary>
        /// <example>CT9M5218000101</example>
        [Required]
        public string IN_CCODE { get; set; }
        /// <summary>
        ///  코드값
        /// </summary>
        /// <example>124</example>
        [Required]
        public Int64 IN_QIDX { get; set; }

    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ
    {
        public Int32 Score { get; set; }
        public string T_Answer1 { get; set; }
        public string T_Answer2 { get; set; }
        public string T_Answer3 { get; set; }
        public string T_WrongAnswer { get; set; }
        public string T_Etc { get; set; }
        public string T_Comment { get; set; }

    }


    #endregion

    #region 밀크T 채점관리 - 첨삭 가이드 점수 저장
    public class P_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_SCORE
    {
        /// <summary>
        /// 학생 아이디 
        /// </summary>
        /// <example>koo5</example>
        [Required]
        public string IN_USERID { get; set; }
        /// <summary>
        /// 첨삭 코드 
        /// </summary>
        /// <example>CT9K52180001</example>
        [Required]
        public string IN_CCODE { get; set; }
        /// <summary>
        ///  코드값
        /// </summary>
        /// <example>124</example>
        [Required]
        public Int64 IN_QIDX { get; set; }
        /// <summary>
        ///  점수
        /// </summary>
        /// <example>90</example>
        [Required]
        public Int32 IN_SCORE { get; set; }

        public string IN_NEWYN { get; set; }
    }
    #endregion

}
