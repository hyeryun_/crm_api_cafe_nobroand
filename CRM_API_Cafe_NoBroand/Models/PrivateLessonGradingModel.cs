﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CRM_API_Cafe_NoBroand.Models
{
    public class PrivateLessonGradingModel
    {
    }


    #region 밀크T 첨삭 교사 채점 화면 - 공통 파람 ( ID.PW )
    public class P_MODEL_Global_PrivateLessonGrading_type01
    {
        /// <summary>
        /// 학생ID
        /// </summary>
        /// <example>xprk5</example>
        [Required]
        public string IN_USERID { get; set; }
        /// <summary>
        /// 차시코드
        /// </summary>
        /// <example>CT9M52180002</example>
        [Required]
        public string IN_CCODE { get; set; }
    }
    #endregion

    #region 밀크T 첨삭 교사 채점 화면 - 문제 
    public class D_MODEL_CHEOMSAK_LECTURE_QUESTION_INFO
    {
        public D_MODEL_USP_APP_CHEOMSAK_LECTURE_READ Lecture { get; set; }
        public List<D_MODEL_CHEOMSAK_QUESTION_INFO> Question { get; set; }

    }

    public class D_MODEL_CHEOMSAK_QUESTION_INFO
    {
        public int qIdx { get; set; }
        public string SType { get; set; } // 검수상태 / 채점상태 (제출:S, 진행:P, 완료:C)
        public string qWUrl { get; set; } // 문항
        public string DataUri { get; set; } // 학습생 답안(통이미지)
        public string Datatext { get; set; } // AI 텍스트 답안        
        public string Correct { get; set; } // 점수 (O, X, L)
        public string AiTutorAnswer { get; set; } // AI 튜터 모범답안
        public string AiTutorComment { get; set; } // AI 튜터 해설
        public string AiTutorCorrect { get; set; } // AI 튜터 정답(정답/오답/설명부족)
        public string S_Answer1 { get; set; } // 채점기준(상)
        public string S_Answer2 { get; set; } // 채점기준(중)
        public string S_Answer3 { get; set; } // 채점기준(하)
        public string VideoUrl { get; set; } // 해설강의       
        public List<D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ_NEW> Guides { get; set; } // 가이드 정보

    }

    public class D_MODEL_USP_APP_CHEOMSAK_LECTURE_READ   /* 차시 정보  */
    {
        public string cCode { get; set; }
        public int mIdx { get; set; }
        public int Grade { get; set; }
        public int Term { get; set; }
        public string Subject { get; set; }
        public string SubjectCode { get; set; }
        public string Unit { get; set; }
        public string QSubject { get; set; }
        public string Mode { get; set; }
        public string TeacherName { get; set; }
        public int Score { get; set; }
        public string EditsType { get; set; }

    }   
    public class D_MODEL_USP_APP_CHEOMSAK_LECTURE_QUESTION_READ   /* 문제 정보  */
    { 
        public string SType { get; set; }
        public int QuestionNum { get; set; }
        public int qIdx { get; set; }
        public string qHUrl { get; set; }
        public string qWUrl { get; set; }
        public string VideoUrl { get; set; }
        public string S_Answer1 { get; set; }
        public string S_Answer2 { get; set; }
        public string S_Answer3 { get; set; }
        public string VoiceA { get; set; }
        public string VoiceB { get; set; }
        public string VoiceC { get; set; }
        public string VoiceYn { get; set; }
        public string TeacherComment { get; set; }
        public int boxcnt { get; set; }
        public string boxtype { get; set; }

    }

    public class D_MODEL_USP_EDITS_CHEOMSAK_TEACHER_GUIDE_READ_NEW
    {
        public int mIdx { get; set; }
        public int qIdx { get; set; }
        public int Score { get; set; }
        public string EvalYN { get; set; }
        public string MarkYN { get; set; }
        public string GuideType { get; set; }
        public int GuideNum { get; set; }
        public int GuideScore { get; set; }
        public string GuideComment { get; set; }
    }

    public class D_MODEL_USP_APP_CHEOMSAK_ANSWER_USERINFO_READ
    {
        /// <summary>
        /// 차시 idx
        /// </summary>
        /// <example>231</example>
        public int mIdx { get; set; }
        /// <summary>
        /// 차시문제 idx
        /// </summary>
        /// <example>10110</example>
        public int qIdx { get; set; }
        /// <summary>
        /// 데이터 타입 ( TXT / IMG / MP3 )
        /// </summary>
        /// <example> IMG </example>
        public string DataTyp { get; set; }
        /// <summary>
        /// 데이터 URL
        /// </summary>
        /// <example> /XXXX/XXXXXX/XXXX/하하호호.JPG </example>
        public string DataUri { get; set; }
        /// <summary>
        /// 채점 상태 (제출완료:S/채점진행:P/채점완료:C)
        /// </summary>
        /// <example> S </example>
        public string Status { get; set; }
        /// <summary>
        /// 텍스트 답안
        /// </summary>
        /// <example> S </example>
        public string datatext { get; set; }
        /// <summary>
        /// 답안 라인수
        /// </summary>
        /// <example> 5 </example>
        public int linecnt { get; set; }
        /// <summary>
        /// 정오답 ( O : 정답 / X : 오답 / L : 설명부족 )
        /// </summary>
        /// <example> 5 </example>
        public string correct { get; set; }
        /// <summary>
        /// AI 튜터 모범답안
        /// </summary>
        /// <example> 공유 </example>
        public string aitutoranswer { get; set; }
        /// <summary>
        /// AI 튜터 해설
        /// </summary>
        /// <example> 멋짐 </example>
        public string aitutorcomment { get; set; }
        /// <summary>
        /// AI 튜터 해설
        /// </summary>
        /// <example> raindev </example>
        public string aitutorcorrect { get; set; }
    }
    #endregion

    #region 밀크T 첨삭 교사 채점 화면 - 채점/검수 진행 상태값변경
    public class P_MODEL_USP_EDITS_CHEOMSAK_STATUS
    {
        public string IN_USERID { get; set; }
        public string IN_CCODE { get; set; }
        public string IN_STATUS { get; set; }
    }
    #endregion


    #region 밀크T 첨삭 교사 채점 화면 - 채점 검수

    public class P_MODEL_USP_EDITS_CHEOMSAK_PARTTEACHER_EVAL
    {
        public string IN_USERID { get; set; }
        public string IN_CCODE { get; set; }
        public Int64 IN_QIDX { get; set; }
        public string IN_TeacherID { get; set; }
    }

    #endregion

}
