﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using CRM_API_Cafe_NoBroand.Helpers;
using CRM_API_Cafe_NoBroand.Models;

using Microsoft.Extensions.Configuration;


namespace CRM_API_Cafe_NoBroand.Services
{
    public interface IAuthenticateService
    {
        //User Authenticate(string username, string password);
        //IEnumerable<User> GetAll();
        Authenticate_loginModel Authenticate(string username, string password, IConfiguration configuration);


    }

    public class AuthenticateService : IAuthenticateService
    {

        private List<User> _users = new List<User>
        {
            new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" }
        };
        

        private readonly AppSettings _appSettings;

        public AppCommonDataAccess _appCommonDataAccess;

        public AuthenticateService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public Authenticate_loginModel Authenticate(string username, string password, IConfiguration configuration)
        {

            //var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);

            var appCommonDataAccess = new AppCommonDataAccess();
            // type 01 
            //appCommonDataAccess.Get_api_authenticate<Authenticate_loginModel>(configuration);

            // type 02
            var user = appCommonDataAccess.Get_api_authenticate_type02(configuration, username, password);
            if (user.Count() < 1) 
                return null;
            if (user[0].TeacherID == null || user[0].TeacherID == "" )
                return null;

            // 정상 적인 경우 jwt 토큰 생성. 
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    //new Claim(ClaimTypes.Name, user.Id.ToString()
                    new Claim(ClaimTypes.Name, user[0].TeacherID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),           //토큰 유효 기간
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user[0].Token = tokenHandler.WriteToken(token);
            //return user;

            return user[0];
        }





    }


}
